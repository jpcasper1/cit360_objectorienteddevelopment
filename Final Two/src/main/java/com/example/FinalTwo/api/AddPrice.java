package com.example.FinalTwo.api;

import com.example.FinalTwo.dbclasses.Customer;
import com.example.FinalTwo.dbclasses.DatabaseInterface;
import com.example.FinalTwo.dbclasses.Location;
import com.example.FinalTwo.dbclasses.Product;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(name = "AddPrice", urlPatterns = "/api/addPrice")
public class AddPrice extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        Enumeration names = request.getParameterNames();
        if (names.hasMoreElements()){
            System.out.println(names.nextElement().toString());
        }

        if(customer != null) {
            DatabaseInterface db = DatabaseInterface.getInstance();
            Integer locationid = Integer.parseInt(request.getParameter("locationid"));
            Integer productid = Integer.parseInt(request.getParameter("productid"));
            System.out.println(locationid);
            System.out.println(productid);
            Product product = db.getProductById(productid);
            Location location = db.getLocationById(locationid);
            //db.createCustomer(name,phone,username,password);
            db.addPriceToProduct(product,location, Float.parseFloat(request.getParameter("price")));
            out.print("Succesful");
        } else {
           response.sendRedirect("/");
        }


    }
}
