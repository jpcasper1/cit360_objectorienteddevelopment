package com.example.FinalTwo.api;

import com.example.FinalTwo.dbclasses.DatabaseInterface;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "createUser", value = "/api/createUser")
public class createUser extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();

        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        DatabaseInterface db = DatabaseInterface.getInstance();
        db.createCustomer(name,phone,username,password);
        response.sendRedirect("/index.html");


    }//DatabaseInterface t = DatabaseInterface.getInstance();
}