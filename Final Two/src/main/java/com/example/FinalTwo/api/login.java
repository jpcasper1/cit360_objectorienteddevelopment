package com.example.FinalTwo.api;

import com.example.FinalTwo.dbclasses.Customer;
import com.example.FinalTwo.dbclasses.DatabaseInterface;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "login", value = "/api/login")
public class login extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Customer customer = DatabaseInterface.getInstance().getCustomerByUsername(username,password);


        if (customer != null){
            session.setAttribute("authenticated",1);
            session.setAttribute("customer",customer);
            response.sendRedirect("/myproducts.jsp");
        } else {
            out.print("False");
        }

    }
}