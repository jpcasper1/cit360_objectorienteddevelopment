package com.example.FinalTwo.api;

import com.example.FinalTwo.dbclasses.Customer;
import com.example.FinalTwo.dbclasses.DatabaseInterface;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "logout", value = "/api/logout")
public class logout extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();
        session.invalidate();
        response.sendRedirect("/");

    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();
        session.invalidate();
        response.sendRedirect("/");

    }
}