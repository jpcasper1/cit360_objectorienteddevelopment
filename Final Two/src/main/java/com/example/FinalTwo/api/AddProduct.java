package com.example.FinalTwo.api;

import com.example.FinalTwo.dbclasses.Customer;
import com.example.FinalTwo.dbclasses.DatabaseInterface;
import com.example.FinalTwo.dbclasses.Location;
import com.example.FinalTwo.dbclasses.Product;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(name = "AddProduct", urlPatterns = "/api/addProduct")
public class AddProduct extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set Content Response Type
        System.out.println("Called the DO POST");
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();

        Customer customer = (Customer) session.getAttribute("customer");

        if(customer != null) {

            String name = request.getParameter("name");
            String unit_name = request.getParameter("unit_name");
            Float unit_value = Float.parseFloat(request.getParameter("unit_value"));

            DatabaseInterface db = DatabaseInterface.getInstance();
            //db.createCustomer(name,phone,username,password);
            Product product = db.addProduct(customer,name, unit_name, unit_value);

            Enumeration e = request.getParameterNames();
            while(e.hasMoreElements()){
                String pname = e.nextElement().toString();
                if(pname.startsWith("loc")){
                    Location loc = db.getLocationById(Integer.parseInt(pname.split("-")[1]));
                    db.addPriceToProduct(product,loc,null);
                }
            }
            response.sendRedirect("/myproducts.jsp");
        } else {
            response.sendRedirect("/");
        }


    }
}
