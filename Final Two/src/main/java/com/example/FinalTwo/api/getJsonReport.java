package com.example.FinalTwo.api;

import com.example.FinalTwo.dbclasses.Customer;
import com.example.FinalTwo.dbclasses.DatabaseInterface;
import com.example.FinalTwo.dbclasses.Location;
import com.example.FinalTwo.dbclasses.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;


@WebServlet(name = "getJsonReport", value = "/api/getJsonReport")
public class getJsonReport extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");

        if(customer != null) {
            out.print(convertToJSON(customer));
        } else {
           response.sendRedirect("/");
        }

    }

    public static String convertToJSON(Object obj){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try    {
            s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }
}