package com.example.FinalTwo.extras;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class json {
    public static String convertToJSON(Object obj){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try    {
            s = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }
}
