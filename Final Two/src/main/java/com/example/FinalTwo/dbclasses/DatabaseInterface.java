package com.example.FinalTwo.dbclasses;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;


public class DatabaseInterface {

    SessionFactory factory = null;
    Session session = null;

    private static DatabaseInterface single_instance = null;

    private DatabaseInterface()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** To Create a single instance connection */
    public static DatabaseInterface getInstance()
    {
        if (single_instance == null) {
            single_instance = new DatabaseInterface();
        }

        return single_instance;
    }


    public void addPriceToProduct(Product product,Location location,Float price){
  try{
                session = factory.openSession();
                session.getTransaction().begin();

                Price dbprice = new Price();
      dbprice.setPrice(price);
      dbprice.setProduct(product);
      dbprice.setLocation(location);
      session.save(dbprice);
                // database calling code here

                session.getTransaction().commit();
                session.close();
            } catch (Exception e) {
                e.printStackTrace();
                session.getTransaction().rollback();
                session.close();
            }

    }

    public Location getLocationById(Integer id){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String hql = "from com.example.FinalTwo.dbclasses.Location where id= :id";
            Location location = (Location) session.createQuery(hql).setParameter("id",id).getSingleResult();
            session.getTransaction().commit();
            session.close();
            return location;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }

    public List<Location> getAllLocations(){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String hql = "from com.example.FinalTwo.dbclasses.Location";
            List<Location> locations = (List<Location>) session.createQuery(hql).getResultList();
            session.close();
            return locations;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }

    public Product addProduct(Customer customer,String name, String unit_name, Float unit_value){
        Product product = new Product();
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            product.setName(name);
            product.setUnitName(unit_name);
            product.setUnitValue(unit_value);
            product.setCustomer(customer);

            session.save(product);

            session.getTransaction().commit();
            session.close();
            return product;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }
    public Product getProductById(Integer id){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String hql = "from com.example.FinalTwo.dbclasses.Product where id= :id";
            Product product = (Product) session.createQuery(hql).setParameter("id",id).getSingleResult();
            session.getTransaction().commit();
            session.close();
            return product;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }

    public Customer createCustomer(String name, String phone, String username, String password){
        Customer customer = new Customer();
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            customer.setName(name);
            customer.setPhone(phone);
            customer.setUsername(username);
            customer.setPassword(password);

            session.save(customer);

            session.getTransaction().commit();
            session.close();
            return customer;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }

    public Customer getCustomerByUsername(String username, String password){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String hql = "from com.example.FinalTwo.dbclasses.Customer AS c where username= :username and password= :password";
            Customer customer = (Customer) session.createQuery(hql).setParameter("username",username).setParameter("password",password).getSingleResult();
            session.getTransaction().commit();
            session.close();
            System.out.println("Successfully found user by "+username);
            return customer;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }
    public Customer getCustomerById(Integer id){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String hql = "from com.example.FinalTwo.dbclasses.Customer AS c where id= :id";
            Customer customer = (Customer) session.createQuery(hql).setParameter("id",id).getSingleResult();
            session.getTransaction().commit();
            session.close();
            System.out.println("Successfully found user by "+id);
            return customer;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return null;
    }

    /** Opens a Session construct for multiple instances, requires closure*/

}