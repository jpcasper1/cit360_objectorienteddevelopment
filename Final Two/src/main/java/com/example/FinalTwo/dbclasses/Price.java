package com.example.FinalTwo.dbclasses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.mapping.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;
import java.sql.Date;


/** Begin mapping to database */
@Entity
@Table(name = "pricetable")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "price")
    private Float price;

    @ManyToOne
    @JoinColumn(name="product_id",nullable=false)
    @OrderBy("datetime")
    @JsonIgnore
    private Product product;

    @ManyToOne
    @JoinColumn(name = "location_id",referencedColumnName = "id",nullable=false)
    private Location location;

    public Price() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}