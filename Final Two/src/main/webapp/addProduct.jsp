<%@ page import="com.example.FinalTwo.dbclasses.Customer" %>
<%@ page import="com.example.FinalTwo.dbclasses.DatabaseInterface" %>
<%@ page import="com.example.FinalTwo.dbclasses.Location" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="ie=edge" http-equiv=X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pricebook Adding Product Page</title>

</head>
<body>
<%
    Customer customer = (Customer) session.getAttribute("customer");
    if (customer != null) { %>
        <h1>Add Product and price:</h1>
        <br><br>

        <form action="api/addProduct" method="post">
            <p>Product Name:  <input name="name" type="name" /></p>
            <p>Measurement Name:   <input name="unit_name" type="text" /></p>
            <p>Measurement Count:  <input name="unit_value" type="number" /></p>

            <br><br>
        <%
            for(Location l: DatabaseInterface.getInstance().getAllLocations()){
                out.print("<div><span><input type='checkbox' name='loc-"+l.getId()+"'>"+l.getName()+", "+l.getZipCode()+"</div>");
            }
        %>
            <input type="submit" value="Add Product" />
        </form>
    <% } else { %>
        <h1>You must login to see this page</h1>
    <% } %>
</body>
</html>