<%@ page import="org.hibernate.Hibernate" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.example.FinalTwo.dbclasses.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="ie=edge" http-equiv=X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pricebook Listing Product Page</title>

</head>
<body>

<%--Header--%>

<div>
    <div><a href="/addProduct.jsp">Add a Product to your list.</a></div>
    <div><a href="/api/getJsonReport" target="_blank">Get Account Data</a></div>
    <div><a href="/api/logout">Logout</a></div>
</div>


<center>
    <h1>Enter Prices</h1>
<%
    Customer customer = (Customer) session.getAttribute("customer");
    if (customer != null) {
        customer = DatabaseInterface.getInstance().getCustomerById(customer.getId());
        session.setAttribute("customer",customer);
        List<Location> locations = new ArrayList<>();
        List<Integer> total_locations = new ArrayList<>();
        for(Product product:customer.getProducts()) {
            for (Price price : product.getPrices()) {
                Integer locID = price.getLocation().getId();
                if (!total_locations.contains(locID)) {
                    total_locations.add(locID);
                    locations.add(price.getLocation());
                }
            }
        }
%>
<table>
    <tr>
        <th></th>
        <% for(Location l:locations){
            out.print("<th>"+l.getName()+"("+l.getZipCode()+")</th>");
        } %>
    </tr>
    <%
        for(Product product: customer.getProducts()){
            out.print("<tr>");
            out.print("<td>"+product.getName()+": "+product.getUnitValue()+" "+product.getUnitName()+"</td>");
//            List<Integer> finished_locations = new ArrayList<>();
//            for(Price price : product.getPrices()){
//                Integer locID = price.getLocation().getId();
//                if(!finished_locations.contains(locID)){
//                    finished_locations.add(locID);
//                    out.print("<td><input id='"+product.getId()+"-"+locID+"' onfocusout='check(event)' type='number' value='"+price.getPrice()+"'></input></td>");
//                }
//            }
            for(Integer locid: total_locations){
                boolean done = false;
                for(Price price : product.getPrices()){
                    Integer locID = price.getLocation().getId();
                    if(locID.equals(locid)){
                        out.print("<td><input id='"+product.getId()+"-"+locID+"' onfocusout='check(event)' type='number' value='"+price.getPrice()+"'></input></td>");
                        done = true;
                        break;
                    }
                } if(!done){
                    out.print("<td><input id='"+product.getId()+"-"+locid+"' onfocusout='check(event)' type='number' value=''></input></td>");
                }

            }
            out.print("</tr>");
        }
    %>

</table>

<% } else { %>
<h1>You must login to see this page</h1>
<% } %>
</center>
</body>
<script>
    function check(e){
        productid = parseInt(e.target.id.split('-')[0]);
        locationid = parseInt(e.target.id.split('-')[1]);
        makePostRequest("/api/addPrice",{locationid:locationid,productid:productid,price:e.target.value})
    }

    function makePostRequest(url,data){
        const XHR = new XMLHttpRequest();
        let urlEncodedData = "",
            urlEncodedDataPairs = [],
            name;


        for( name in data ){
            urlEncodedDataPairs.push(encodeURIComponent(name)+'='+encodeURIComponent(data[name]))
        }
        urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g,'+')

        XHR.addEventListener('load',function(event){
            console.log(event)
        })

        XHR.open('POST',url);
        XHR.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
        XHR.send(urlEncodedData)
    }

</script>
<style>
    table,tr,td,th {
        padding:2px;
    }

    input[type=number] {
        border:none;
        border-bottom: 3px solid gray;
        border-top-left-radius: 2px;
        border-top-right-radius: 2px;
        background-color: lightgray;
    }
</style>
</html>