-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: pricebookapp
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Username_UNIQUE` (`username`),
  UNIQUE KEY `idcustomers_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='customers table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Corbin','6032471709','corbin','password'),(2,'Joann','9163165671','jc','password'),(3,'Fred','9999999999','fred','barney'),(4,'Sarah','123456456','sz','password');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Walmart','03038'),(2,'Costco','03038'),(3,'Shaws','03038'),(4,'Hannafords','03038'),(5,'Sams Club','95682'),(6,'Safeway','95682'),(7,'Winco','95682'),(8,'Raleys','95682');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pricetable`
--

DROP TABLE IF EXISTS `pricetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pricetable` (
  `id` int NOT NULL AUTO_INCREMENT,
  `price` float DEFAULT NULL,
  `datetime` datetime DEFAULT CURRENT_TIMESTAMP,
  `location_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idLocationn_idx` (`location_id`),
  KEY `idproduct_idx` (`product_id`),
  CONSTRAINT `idLocation` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`),
  CONSTRAINT `idproduct` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pricetable`
--

LOCK TABLES `pricetable` WRITE;
/*!40000 ALTER TABLE `pricetable` DISABLE KEYS */;
INSERT INTO `pricetable` VALUES (1,5.55,'2021-04-02 20:19:26',1,2),(2,5.55,'2021-04-02 20:19:32',1,2),(3,5.55,'2021-04-02 20:19:33',1,2),(4,5.55,'2021-04-02 20:19:33',1,2),(5,5.55,'2021-04-02 20:19:33',1,2),(6,5.55,'2021-04-02 20:19:33',1,2),(7,5.55,'2021-04-02 20:19:47',1,2),(8,5.55,'2021-04-02 20:19:48',1,2),(9,5.55,'2021-04-02 20:19:48',1,2),(10,5.55,'2021-04-02 20:19:48',1,2),(11,5.55,'2021-04-02 20:19:48',1,2),(12,5.55,'2021-04-02 20:19:48',1,2),(13,5.5,'2021-04-02 20:20:06',1,2),(14,1,'2021-04-02 20:40:03',1,3),(15,1,'2021-04-02 20:40:28',1,2),(16,100,'2021-04-02 20:43:17',1,2),(17,100,'2021-04-02 20:51:54',2,2),(18,100,'2021-04-02 20:52:09',2,1),(19,100,'2021-04-02 20:52:25',1,1),(20,100,'2021-04-02 20:52:29',2,1),(21,10,'2021-04-02 21:18:43',2,1),(22,2.5,'2021-04-02 21:19:22',2,1),(23,100,'2021-04-02 22:00:24',1,1),(24,5,'2021-04-02 22:00:27',1,1),(25,123123,'2021-04-02 22:00:37',1,1),(26,50,'2021-04-02 22:00:44',1,1),(27,123,'2021-04-02 22:00:59',1,2),(28,123,'2021-04-02 22:01:04',1,2),(29,123,'2021-04-02 22:01:08',1,2),(30,123,'2021-04-02 22:01:57',1,2),(31,1,'2021-04-02 22:09:26',1,9),(32,2,'2021-04-02 22:09:27',1,4),(33,3,'2021-04-02 22:09:28',1,8),(34,4,'2021-04-02 22:09:29',1,5),(35,5,'2021-04-02 22:09:30',1,7),(36,6,'2021-04-02 22:09:30',1,6),(37,5,'2021-04-02 22:15:15',2,7),(38,-4,'2021-04-02 22:15:26',2,6),(39,2,'2021-04-02 22:15:32',2,3),(40,NULL,'2021-04-04 19:44:44',1,11),(41,NULL,'2021-04-04 19:44:44',2,11),(42,NULL,'2021-04-04 19:44:44',3,11),(43,NULL,'2021-04-04 19:44:44',4,11),(44,NULL,'2021-04-04 19:44:44',5,11),(45,NULL,'2021-04-04 19:44:44',6,11),(46,NULL,'2021-04-04 19:44:44',7,11),(47,NULL,'2021-04-04 19:44:44',8,11),(48,NULL,'2021-04-04 19:45:54',1,12),(49,NULL,'2021-04-04 19:45:54',3,12),(50,NULL,'2021-04-04 20:41:41',1,13),(51,123,'2021-04-04 20:41:49',1,13),(52,123,'2021-04-04 20:41:52',1,13),(53,NULL,'2021-04-04 20:42:45',1,14),(54,NULL,'2021-04-04 20:42:45',3,14),(55,NULL,'2021-04-04 20:42:45',4,14),(56,1,'2021-04-04 20:42:48',1,14),(57,2,'2021-04-04 20:42:49',3,14),(58,3,'2021-04-04 20:42:50',4,14),(59,NULL,'2021-04-04 20:43:40',2,15),(60,10,'2021-04-04 20:43:48',2,15),(61,10,'2021-04-04 20:46:29',2,15),(62,3,'2021-04-04 20:46:32',4,14),(63,10,'2021-04-04 20:49:08',2,15),(64,3,'2021-04-04 20:51:20',4,14);
/*!40000 ALTER TABLE `pricetable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `unit_name` varchar(45) DEFAULT NULL,
  `unit_count` float DEFAULT NULL,
  `customer_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idcustomer_idx` (`customer_id`),
  CONSTRAINT `idcustomer` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Milk','Gallon',1,1),(2,'Eggs','Dozen',1,1),(3,'Waffles','Ounce',12,1),(4,'Pancakes','Ounce',12,1),(5,'Soda','Litre',1,1),(6,'Soda Two','Litre',1,1),(7,'Soda Three','Litre',1,1),(8,'Soda Four','Litre',1,1),(9,'Bread','loaf',1,1),(10,'Triceratops','Babies',12,2),(11,'Test Product','Test',1,1),(12,'Test Product','test',1,2),(13,'Test Product 2','Test 2',123,1),(14,'Waffles','Ounce',12,4),(15,'pancakes','pound',1,4);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-07 12:54:12
